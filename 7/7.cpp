#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

void count_coin(int* c, int& paid, int& used)
{
	paid =  2000 * c[2000] + 1000 * c[1000] + 500 * c[500] + 
			200 * c[200] + 100 * c[100] + 50 * c[50] + 
			20 * c[20] + 10 * c[10] + 5 * c[5] + c[1];
	used =  c[2000] + c[1000] + c[500] + c[200] + c[100] + 
			c[50] + c[20] + c[10] + c[5] + c[1];
	return;
}

void reduce_delta(int* c, int& used, int& delta, int deno)
{
	if (c[deno] >= delta / deno)
	{
		used -= delta / deno;
		delta -= deno * (delta / deno);
	}
	else
	{
		used -= c[deno];
		delta -= deno * c[deno];
	}
	return;
}

int max_coin(int* c, int p)
{
	int cases[4] = {0, -50, -500, -550};
	int max = -1;
	for (int i = 0; i < 4; i++)
	{
		// cout << "i: " << i << endl;
		int paid;
		int used;
		count_coin(c, paid, used);
		int delta = paid - p;
		if (i == 1)
		{
			if (delta + cases[i] < 0 or c[50] == 0)
				continue;
		}
		else if (i == 2)
		{
			if (delta + cases[i] < 0 or c[500] == 0)
				continue;
		}
		else if (i == 3)
		{
			if (delta + cases[i] < 0 or c[500] == 0 or c[50] == 0)
				continue;
		}

		if (i == 1 or i == 2)
			used--;
		else if (i == 3)
			used -= 2;

		delta += cases[i];
		if (delta >= 2000)
			reduce_delta(c, used, delta, 2000);
		if (delta >= 1000)
			reduce_delta(c, used, delta, 1000);
		if (delta >= 500)
		{
			if (i == 2 or i == 3)
			{
				if (c[500] - 1 >= 2 * (delta / 1000))
				{
					used -= 2 * (delta / 1000);
					delta -= 1000 * (delta / 1000);
				}
				else
				{
					used -= 2 * ((c[500] - 1) / 2);
					delta -= 1000 * ((c[500] - 1) / 2);
				}
			}
			else
			{
				if (c[500] >= 2 * (delta / 1000))
				{
					used -= 2 * (delta / 1000);
					delta -= 1000 * (delta / 1000);
				}
				else
				{
					used -= 2 * (c[500] / 2);
					delta -= 1000 * (c[500] / 2);
				}
			}
		}
		if (delta >= 200)
			reduce_delta(c, used, delta, 200);
		if (delta >= 100)
			reduce_delta(c, used, delta, 100);
		if (delta >= 50) 
		{
			if (i == 1 or i == 3)
			{
				if (c[50] - 1 >= 2 * (delta / 100))
				{
					used -= 2 * (delta / 100);
					delta -= 100 * (delta / 100);
				}
				else
				{
					used -= 2 * ((c[50] - 1) / 2);
					delta -= 100 * ((c[50] - 1) / 2);
				}
			}
			else
			{
				if (c[50] >= 2 * (delta / 100))
				{
					used -= 2 * (delta / 100);
					delta -= 100 * (delta / 100);
				}
				else
				{
					used -= 2 * (c[50] / 2);
					delta -= 100 * (c[50] / 2);
				}
			}
		}
		if (delta >= 20)
			reduce_delta(c, used, delta, 20);
		if (delta >= 10)
			reduce_delta(c, used, delta, 10);
		if (delta >= 5)
			reduce_delta(c, used, delta, 5);
		if (delta >= 1)
			reduce_delta(c, used, delta, 1);

		if (delta != 0)
			used = -1;
	//	cout << "used: " << used << endl;
		if (used > max)
			max = used;
	}

	return max;
}

int main()
{
	int t;
	int p;
	int c[2010];
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d%d%d%d%d%d%d%d%d%d", &p, &c[1], &c[5],
				&c[10], &c[20], &c[50], &c[100], &c[200], 
				&c[500], &c[1000], &c[2000]);
		printf("%d\n", max_coin(c, p));
	}

	return 0;
}
