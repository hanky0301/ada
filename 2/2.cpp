#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// global variable "ans"
long long ans = 0;

typedef struct box
{
	int len;
	int wid;
	struct box& operator=(const struct box& box)
	{
		len = box.len;
		wid = box.wid;
	}
	bool operator==(const struct box& box) const
	{
		return (len == box.len and wid == box.wid);
	}
} Box;

bool compare(const Box& box1, const Box& box2)
{
	return (box1.len > box2.len or (box1.len == box2.len and box1.wid > box2.wid));
}

// accumulate ans in the "merge" function
vector<Box> merge(vector<Box>& left, vector<Box>& right)
{
	int cur_left = 0; 	// a cursor on "left"
	int cur_right = 0;	// a cursor on "right"
	vector<Box> result(left.size() + right.size());
	int count = 0;

	while (cur_left < left.size() and cur_right < right.size())
	{
		if (left[cur_left].wid > right[cur_right].wid)
		{
			// box in "left" can contain box in "right"
			ans += right.size() - cur_right;
			result[count] = left[cur_left];
			cur_left++;
			count++;
		}
		else if (left[cur_left].wid == right[cur_right].wid)
		{
			if (left[cur_left].len == right[cur_right].len)
			{
				//	deal with identical boxes
				int org_cur_right = cur_right;
				Box same_box = left[cur_left];
				int count_left = 0, count_right = 0;

				while (cur_left != left.size() and left[cur_left] == same_box)
				{
					count_left++;
					result[count] = left[cur_left];
					cur_left++;
					count++;
				}
				while (cur_right != right.size() and right[cur_right] == same_box)
				{
					count_right++;
					result[count] = right[cur_right];
					cur_right++;
					count++;
				}
				ans += (long long)((right.size() - org_cur_right + count_right) * count_left);
			}
			else
			{
				ans += right.size() - cur_right;
				result[count] = left[cur_left];
				cur_left++;
				count++;
			}
		}
		else
		{
			result[count] = right[cur_right];
			cur_right++;
			count++;
		}
	}

	// append the rest of "left" and "right" to result
	while (cur_left < left.size())
	{
		result[count] = left[cur_left];
		cur_left++;
		count++;
	}
	while (cur_right < right.size())
	{
		result[count] = right[cur_right];
		cur_right++;
		count++;
	}

	return result;
}

// sort the width(shorter edge of a box)
vector<Box> merge_sort(vector<Box>& vec)
{
	if (vec.size() == 1)
		return vec;

	int mid = vec.size() / 2;
	vector<Box> left(vec.begin(), vec.begin() + mid);
	vector<Box> right(vec.begin() + mid, vec.end());

	left = merge_sort(left);
	right = merge_sort(right);
	return merge(left, right);
}

int main()
{
	int t, n;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		ans = 0;
		scanf("%d", &n);
		vector<Box> box(n);
		for (int j = 0; j < n; j++)
		{
			scanf("%d%d", &(box[j].len), &(box[j].wid));
			if (box[j].len < box[j].wid)
				swap(box[j].len, box[j].wid);
		}
		sort(box.begin(), box.end(), compare);
		merge_sort(box);
		printf("%lld\n", ans);
	}

	return 0;
}
