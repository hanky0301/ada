#include <cstdio>
#include <iostream>
#include <list>
#include <vector>
using namespace std;

struct Edge
{
	int index1;
	int index2;
	int weight;
	int flag;
};

struct Vertex
{
	int parent;
	int rank;
};

void MAKE_SET(int index, Vertex v[])
{
	v[index].parent = index;
	v[index].rank = 0;
}

int FIND_SET(int index, Vertex v[])
{
	if (index != v[index].parent)
		v[index].parent = FIND_SET(v[index].parent, v);
	return v[index].parent;
}

void LINK(int index1, int index2, Vertex v[])
{
	if (v[index1].rank > v[index2].rank)
		v[index2].parent = index1;
	else
	{
		v[index1].parent = index2;
		if (v[index1].rank == v[index2].rank)
			v[index2].rank += 1;
	}
	return;
}

void UNION(int index1, int index2, Vertex v[])
{
	LINK(FIND_SET(index1, v), FIND_SET(index2, v), v);
	return;
}

int minimum_OR_spanning_tree(int n, Vertex v[], vector<Edge>& edge_vec)
{
	int min = 0;
	vector<Edge>::iterator it;
	for (int i = 30; i >= 0; i--)
	{
		int groups = n;
		for (int j = 1; j <= n; j++)
			MAKE_SET(j, v);
		for (it = edge_vec.begin(); it != edge_vec.end(); it++)
			if (it->flag == 0 and (it->weight & (1 << i)) == 0)
				if (FIND_SET(it->index1, v) != FIND_SET(it->index2, v))
				{
					UNION(it->index1, it->index2, v);
					groups--;
				}

		if (groups > 1)
			min |= (1 << i);
		else
			for (it = edge_vec.begin(); it != edge_vec.end(); it++)
				if (it->weight & (1 << i))
					it->flag = 1;
	}
	return min;
}

int main()
{
	int t, n, m;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d", &n, &m);
		Vertex v[n + 1];
		vector<Edge> edge_vec(m);
		int count = 0;
		for (int j = 0; j < m; j++)
		{
			Edge e;
			scanf("%d%d%d", &e.index1, &e.index2, &e.weight);
			e.flag = 0;
			if (e.index1 != e.index2)
			{
				edge_vec[count] = e;
				count++;
			}
		}
		edge_vec.resize(count);
		printf("%d\n", minimum_OR_spanning_tree(n, v, edge_vec)); 
	}
	return 0;
}
