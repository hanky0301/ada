#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

int required_airports(int max, int* city, int n)
{
//	cout << "max: " << max << endl;
	int count = 1;
	int start = city[0];
	int pos;

	for (int i = 1; i < n; i++)
	{
		if (city[i] - start > max)
		{	
			// cout << "i: " << i << " ";
			pos = city[i - 1];
			// cout << "pos: " << pos << endl;
			while (i < n)
			{
				if (city[i] - pos > max)
				{
					start = city[i];
					count++;
					break;
				}
				i++;
			}
		}
	}

//	cout << "count: " << count << endl << endl;
	return count;
}

int max_dist(int* city, int n, int k)
{
	int l = 0, r = city[n - 1] - city[0] + 1;
	int ans;
	while (l < r)
	{
		int mid = (l + r) / 2;

		if (required_airports(mid, city, n) <= k)
		{
			r = mid;
			ans = mid;
		}
		else
			l = mid + 1;
	}

	return ans;
}

int main()
{
	int t;
	int n, k;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d", &n, &k);
		int city[n];
		for (int j = 0; j < n; j++)
			scanf("%d", &city[j]);
		sort(city, city + n);
		printf("%d\n", max_dist(city, n, k));
	}

	return 0;
}
