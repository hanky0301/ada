#include <cstdio>

int a[100010];
int b[100010];

int main()
{
	int t;
	int n;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d", &n);
		int max = 0;
		int max_index = 0;
		for (int j = 0; j < n; j++)
		{
			scanf("%d", &a[j]);
			if (a[j] > max)
			{
				max = a[j];
				max_index = j;
			}
		}
		b[max_index] = -1;
		for (int j = max_index + 1; j != max_index; j++)
		{
			if (j == n)
			{
				j = -1;
				continue;
			}
			int ask = (j - 1 != -1 ? j - 1 : n - 1);
			if (a[ask] > a[j])
				b[j] = ask;
			else
				while (a[ask] <= a[j])
				{
					if (b[ask] == -1)
					{
						b[j] = -1;
						break;
					}
					ask = b[ask];
					b[j] = ask;
				}
		}
		for (int j = 0; j < n - 1; j++)
			printf("%d ", b[j] + 1);
		printf("%d\n", b[n - 1] + 1);
	}
	return 0;
}
