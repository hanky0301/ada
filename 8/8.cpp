#include <cstdio>
#include <iostream>
#include <queue>
#include <climits>
using namespace std;

typedef struct index
{
	int i;
	int j;
	int k;
	index() {}
	index(int z, int x, int y): i(z), j(x), k(y) {}
	void assign(int z, int x, int y)
	{
		i = z;
		j = x;
		k = y;
	}
} Index;

typedef struct vertex
{
	int dist;
	int bev;
} Vertex;

char room[100][100][100];
Vertex vertex[100][100][100];

void initialize(int n, const Index& start)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			for (int k = 0; k < n; k++)
			{
				vertex[i][j][k].dist = INT_MAX;
				vertex[i][j][k].bev = 0;
			}

	vertex[start.i][start.j][start.k].dist = 0;
	return;
}

void update_vertex(const Index& ind, int n, queue<Index>& q)
{
	int delta, bev_new;
	for (delta = -1; delta <= 1; delta += 2)
	{
		if (ind.k + delta != (delta < 0 ? -1 : n) and room[ind.i][ind.j][ind.k + delta] != '#')
		{
			if (vertex[ind.i][ind.j][ind.k].dist + 1 < vertex[ind.i][ind.j][ind.k + delta].dist)
			{
				vertex[ind.i][ind.j][ind.k + delta].dist = vertex[ind.i][ind.j][ind.k].dist + 1;
				q.push(Index(ind.i, ind.j, ind.k + delta));
			}
			if (vertex[ind.i][ind.j][ind.k].dist + 1 == vertex[ind.i][ind.j][ind.k + delta].dist)
			{
				bev_new = (room[ind.i][ind.j][ind.k + delta] == 'B');
				if (vertex[ind.i][ind.j][ind.k].bev + bev_new > vertex[ind.i][ind.j][ind.k + delta].bev)
					vertex[ind.i][ind.j][ind.k + delta].bev = vertex[ind.i][ind.j][ind.k].bev + bev_new;
			}
		}
	}
	for (delta = -1; delta <= 1; delta += 2)
	{
		if (ind.j + delta != (delta < 0 ? -1 : n) and room[ind.i][ind.j + delta][ind.k] != '#')
		{
			if (vertex[ind.i][ind.j][ind.k].dist + 1 < vertex[ind.i][ind.j + delta][ind.k].dist)
			{
				vertex[ind.i][ind.j + delta][ind.k].dist = vertex[ind.i][ind.j][ind.k].dist + 1;
				q.push(Index(ind.i, ind.j + delta, ind.k));
			}
			if (vertex[ind.i][ind.j][ind.k].dist + 1 == vertex[ind.i][ind.j + delta][ind.k].dist)
			{
				bev_new = (room[ind.i][ind.j + delta][ind.k] == 'B');
				if (vertex[ind.i][ind.j][ind.k].bev + bev_new > vertex[ind.i][ind.j + delta][ind.k].bev)
					vertex[ind.i][ind.j + delta][ind.k].bev = vertex[ind.i][ind.j][ind.k].bev + bev_new;
			}
		}
	}
	for (delta = -1; delta <= 1; delta += 2)
	{
		if (ind.i + delta != (delta < 0 ? -1 : n) and room[ind.i + delta][ind.j][ind.k] != '#')
		{
			if (vertex[ind.i][ind.j][ind.k].dist + 1 < vertex[ind.i + delta][ind.j][ind.k].dist)
			{
				vertex[ind.i + delta][ind.j][ind.k].dist = vertex[ind.i][ind.j][ind.k].dist + 1;
				q.push(Index(ind.i + delta, ind.j, ind.k));
			}
			if (vertex[ind.i][ind.j][ind.k].dist + 1 == vertex[ind.i + delta][ind.j][ind.k].dist)
			{
				bev_new = (room[ind.i + delta][ind.j][ind.k] == 'B');
				if (vertex[ind.i][ind.j][ind.k].bev + bev_new > vertex[ind.i + delta][ind.j][ind.k].bev)
					vertex[ind.i + delta][ind.j][ind.k].bev = vertex[ind.i][ind.j][ind.k].bev + bev_new;
			}
		}
	}
	return;
}

void shortest_path(int n, const Index& start, const Index& exit)
{
	Index ind;
	initialize(n, start);

	queue<Index> q;
	q.push(start);
	while (q.size() != 0)
	{
		ind = q.front();
		q.pop();
		if (vertex[ind.i][ind.j][ind.k].dist < vertex[exit.i][exit.j][exit.k].dist)
			update_vertex(ind, n, q);
	}
	if (vertex[exit.i][exit.j][exit.k].dist == INT_MAX)
		printf("Fail OAQ\n");
	else
		printf("%d %d\n", vertex[exit.i][exit.j][exit.k].dist, vertex[exit.i][exit.j][exit.k].bev);
	return;
}

int main()
{
	int t, n;
	Index start, exit;
	
	scanf("%d", &t);
	while (t--)
	{
		scanf("%d", &n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				for (int k = 0; k < n; k++)
				{
					scanf(" %c", &room[i][j][k]);
					if (room[i][j][k] == 'S')
						start.assign(i, j, k);
					else if (room[i][j][k] == 'E')
						exit.assign(i, j, k);
				}

		shortest_path(n, start, exit);
	}
	return 0;
}
