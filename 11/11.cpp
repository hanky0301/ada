#include <cstdio>
#include <climits>
using namespace std;

typedef __uint128_t int128;
int128 one_128 = 1;

int popcount(int128 val)
{
	return __builtin_popcountll(val >> 50)
		+ __builtin_popcountll(val & ((one_128 << 50) - 1));
}

int ctz(int128 val)
{
	if((val & ((one_128 << 50) - 1)) == 0)
		return __builtin_ctzll(val >> 50) + 50;
	return __builtin_ctzll(val & ((one_128 << 50) - 1));
}

void bron_kerbosch(int count, int128 P, int128 X, int128 adj[], int& max)
{
	if (!P and !X)
	{
		if (count > max)
			max = count;
		return;
	}
	int count_P = popcount(P);
	if (count_P + count < max)
		return;

	int pivot = ctz(P | X);
	int i;
	while (P & (~adj[pivot]) and count_P + count > max)
	{
		i = ctz(P & (~adj[pivot]));
		if (i == pivot || (P & (~adj[pivot]) & adj[i]))
			bron_kerbosch(count + 1, P & adj[i], X & adj[i], adj, max);
		P ^= (one_128 << i);
		count_P--;
		X |= (one_128 << i);
	}
	return;
}

int max_clique_size(int n, int128 adj[])
{
	int max = 0;
	int128 P = (one_128 << n) - 1;
	int128 X = 0;
	bron_kerbosch(0, P, X, adj, max);
	return max;
}

int main()
{
	int t;
	int n, m;
	int v1, v2;

	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d", &n, &m);
		int128 adj[n];
		for (int j = 0; j < n; j++)
		{
			adj[j] = (one_128 << n) - 1;
			adj[j] ^= (one_128 << j);
		}
		for (int j = 0; j < m; j++)
		{
			scanf("%d%d", &v1, &v2);
			adj[v1] ^= (one_128 << v2);
			adj[v2] ^= (one_128 << v1);
		}
		printf("%d\n", max_clique_size(n, adj));
	}
	return 0;
}
