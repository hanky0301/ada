#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

typedef struct player
{
	unsigned long long goal;
	bool has_reached;
	int reached_event;
	unsigned long long gold;
	player()
	{
		reached_event = -2;
		gold = 0;
	}
} Player;

typedef struct event
{
	int l;
	int r;
	unsigned long long v;
} Event;

typedef struct block
{
	int owner;
	unsigned long long cur;
} Block;

// global array cur_player
unsigned long long cur_player[100001];
// global array flag
int flag[100001];

// btc: blocks to check
// etc: events to check (from event[start] to event[mid])
void cal_gold(vector<int>& btc, int start, int mid, const vector<Event>& event, 
			  vector<Block>& block, vector<Player>& player)
{
	// process the events into block.cur
	unsigned long long count = 0;
	for (int i = start; i <= mid; i++)
	{
		vector<int>::iterator low1, low2;
		low1 = lower_bound(btc.begin(), btc.end(), event[i].l);
		low2 = lower_bound(btc.begin(), btc.end(), event[i].r + 1);
		if (low1 != btc.end())
			block[btc[low1 - btc.begin()]].cur += event[i].v;
		if (low2 != btc.end())
			block[btc[low2 - btc.begin()]].cur -= event[i].v;
	}

	// reset cur_player and flag
	for (int i = 0; i < btc.size(); i++)
	{
		cur_player[block[btc[i]].owner] = 0;
		flag[block[btc[i]].owner] = 0;
	}
	// calculate the gold gain in each block in these events
	// and whether the owner of the block is success
	for (int i = 0; i < btc.size(); i++)
	{
		cur_player[block[btc[i]].owner] += block[btc[i]].cur + count;
		count += block[btc[i]].cur;
		block[btc[i]].cur = 0;
		if (cur_player[block[btc[i]].owner] + player[block[btc[i]].owner].gold >=
				player[block[btc[i]].owner].goal)
		{
			player[block[btc[i]].owner].has_reached = true;
			player[block[btc[i]].owner].reached_event = mid;
		}
		else
			player[block[btc[i]].owner].has_reached = false;
	}

	// add the gold gain in these events to player.gold if they failed 
	for (int i = 0; i < btc.size(); i++)
	{
		if (player[block[btc[i]].owner].has_reached == false)
		{
			if (flag[block[btc[i]].owner] == 0)
			{
				player[block[btc[i]].owner].gold += cur_player[block[btc[i]].owner];
				flag[block[btc[i]].owner] = 1;
			}
		}
	}

	return;
}

// divide and conquer
void reach_goal(vector<int>& btc, int start, int end, const vector<Event>& event, 
				vector<Block>& block, vector<Player>& player)
{
	if (btc.size() == 0)
		return;

	int mid = (start + end) / 2;
	if (start == end)
	{
		// calculate the gold in this event for the last time
		cal_gold(btc, start, mid, event, block, player);
		return;
	}

	cal_gold(btc, start, mid, event, block, player);
	vector<int> btc_success(btc.size());	// seccess blocks
	vector<int> btc_failed(btc.size()); 	// failed blocks 
	int count_s = 0;
	int count_f = 0;
	for (int i = 0; i < btc.size(); i++)
		if (player[block[btc[i]].owner].has_reached)
		{
			btc_success[count_s] = btc[i];
			count_s++;
		}
		else
		{
			btc_failed[count_f] = btc[i];
			count_f++;
		}
	btc_success.resize(count_s);
	btc_failed.resize(count_f);

	reach_goal(btc_success, start, mid, event, block, player);
	reach_goal(btc_failed, mid + 1, end, event, block, player);
	
	return;
}

int main()
{
	int t;
	int n, m, q;
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d%d", &n, &m, &q);
		vector<Player> player(n);
		vector<Block> block(m);
		vector<Event> event(q);
		
		int tmp, tmp1;
		unsigned long long tmp2;

		for (int j = 0; j < n; j++)
			scanf("%lld", &(player[j].goal));
		for (int j = 0; j < m; j++)
		{
			scanf("%d", &tmp);
			block[j].owner = tmp - 1;
		}
		for (int j = 0; j < q; j++)
		{
			scanf("%d%d%lld", &tmp, &tmp1, &tmp2);
			event[j].l = tmp - 1;
			event[j].r = tmp1 - 1;
			event[j].v = tmp2;
		}

		vector<int> btc(m);
		for (int i = 0; i < m; i++)
			btc[i] = i;
		reach_goal(btc, 0, q - 1, event, block, player);
		for (int i = 0; i < player.size() - 1; i++)
			printf("%d ", player[i].reached_event + 1);
		printf("%d\n", player[player.size() - 1].reached_event + 1);
	}
	
	return 0;
}

/*
1
3 5 4
15 10 50
1 2 3 1 2
2 5 3
1 4 2
2 4 5
1 5 3
*/
