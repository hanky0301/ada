#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

int lcslen[2010][2010];
char lcs[2010][2010];
char cur[2010][2010];

// calculate the length of lcs
void lcs_len(char* a, char* b)
{
	int len_a = strlen(a);
	int len_b = strlen(b);
	for (int i = 0; i < len_a; i++)
		lcslen[0][i] = 0;
	for (int i = 0; i < len_b; i++)
		lcslen[i][0] = 0;
	
	for (int i = 1; i < len_a; i++)
		for (int j = 1; j < len_b; j++)
			if (a[i] == b[j])
			{
				// inherit the value from lcslen[i - 1, j + 1] and add 1
				lcslen[i][j] = lcslen[i - 1][j - 1] + 1;
				lcs[i][j] = '\\';
				cur[i][j] = a[i];
			}
			else if (lcslen[i - 1][j] > lcslen[i][j - 1])
			{
				// inherit the value from lcslen[i - 1, j]
				lcslen[i][j] = lcslen[i - 1][j];
				lcs[i][j] = '|';
				cur[i][j] = cur[i - 1][j];
			}
			else if (lcslen[i - 1][j] < lcslen[i][j - 1])
			{
				// inherit the value from lcslen[i, j - 1]
				lcslen[i][j] = lcslen[i][j - 1];
				lcs[i][j] = '-';
				cur[i][j] = cur[i][j - 1];
			}
			else
			{
				if (cur[i - 1][j] < cur[i][j - 1])
				{
					// select the lexicoraphically smaller one
					lcslen[i][j] = lcslen[i - 1][j];
					lcs[i][j] = '|';
					cur[i][j] = cur[i - 1][j];
				}
				else if (cur[i - 1][j] > cur[i][j - 1])
				{
					lcslen[i][j] = lcslen[i][j - 1];
					lcs[i][j] = '-';
					cur[i][j] = cur[i][j - 1];
				}
				else
				{
					// 選對到原字串有相同者
					if (cur[i - 1][j] == b[j])
					{
						lcslen[i][j] = lcslen[i - 1][j];
						lcs[i][j] = '|';
						cur[i][j] = cur[i - 1][j];
					}
					else
					{
						lcslen[i][j] = lcslen[i][j - 1];
						lcs[i][j] = '-';
						cur[i][j] = cur[i][j - 1];
					}
				}
			}
	return;
}

// follow the directions stored in lcs
void print_lcs(char* a, int i, int j)
{
	if (i == 0 or j == 0)
		return;
	if (lcs[i][j] == '\\')
	{
		printf("%c", a[i]);
		print_lcs(a, i - 1, j - 1);
	}
	else if (lcs[i][j] == '|')
		print_lcs(a, i - 1, j);
	else
		print_lcs(a, i, j - 1);
	return;
}

void reverse(char* str)
{
	int len = strlen(str);
	for (int i = 0; i < len / 2; i++)
		swap(str[i], str[len - 1 - i]);
	return;
}

int main()
{
	int t;
	scanf("%d", &t);
	char a[2010];
	char b[2010];
	int len_a, len_b;
	for (int i = 0; i < t; i++)
	{
		scanf("%s%s", a, b);
		len_a = strlen(a);
		len_b = strlen(b);
		a[len_a] = '0';
		a[len_a + 1] = '\0';
		b[len_b] = '0';
		b[len_b + 1] = '\0';
		reverse(a);
		reverse(b);

		lcs_len(a, b);
		print_lcs(a, len_a, len_b);
		printf("\n");
	}
	return 0;
}
