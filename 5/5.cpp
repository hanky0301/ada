#include <cstdio>
#include <iostream>
#define MAX 1000000007
using namespace std;

char board[20][20];

void build_power(int* power, int m)
{
	power[m - 1] = 1;
	for (int i = m - 2; i >= 0 ; i--)
		power[i] = power[i + 1] << 1;
	return;
}

int fill_board(int n, int m)
{
	int power[m];
	build_power(power, m);
	int dp[n + 1][m][power[0] << 1];
	for (int i = 0; i <= n; i++)
		for (int j = 0; j < m; j++)
			for (int k = 0; k < power[0] << 1; k++)
				dp[i][j][k] = 0;

	dp[0][0][0] = 1;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
			for (int k = 0; k < power[0] << 1; k++)
			{	
				if (dp[i][j][k] == 0)
					continue;
				if (board[i][j] == 'X')
				{
					dp[i + (j + 1) / m][(j + 1) % m][k] += dp[i][j][k];
					dp[i + (j + 1) / m][(j + 1) % m][k] %= MAX;
				}
				else if ((k & power[j]) != 0)
				{
					dp[i + (j + 1) / m][(j + 1) % m][k ^ power[j]] += dp[i][j][k];
					dp[i + (j + 1) / m][(j + 1) % m][k ^ power[j]] %= MAX;
				}
				else
				{
					dp[i + (j + 1) / m][(j + 1) % m][k] += dp[i][j][k];
					dp[i + (j + 1) / m][(j + 1) % m][k] %= MAX;

					if (board[i + 1][j] != 'X' and (k & power[j]) == 0)
					{
						dp[i + (j + 1) / m][(j + 1) % m][k | power[j]] += dp[i][j][k];
						dp[i + (j + 1) / m][(j + 1) % m][k | power[j]] %= MAX;
					}

					if (board[i][j + 1] != 'X' and (k & power[j + 1]) == 0)
					{
						dp[i + (j + 2) / m][(j + 2) % m][k] += dp[i][j][k];
						dp[i + (j + 2) / m][(j + 2) % m][k] %= MAX;
					}

					if (board[i][j + 1] != 'X' and board[i + 1][j] != 'X' and board[i + 1][j + 1] != 'X' and (k & power[j + 1]) == 0)
					{
						dp[i + (j + 2) / m][(j + 2) % m][k | power[j] | power[j + 1]] += dp[i][j][k];
						dp[i + (j + 2) / m][(j + 2) % m][k | power[j] | power[j + 1]] %= MAX;
					}
				}
			}
		}
	return dp[n][0][0] % MAX;
}

int main()
{
	int t;
	int n, m;
	char tmp[20];
	scanf("%d", &t);
	for (int i = 0; i < t; i++)
	{
		scanf("%d%d", &n, &m);
		for (int j = 0; j < n; j++)
		{	
			scanf("%s", tmp);
			for (int k = 0; k < m; k++)
				board[j][k] = tmp[k];
			board[j][m] = 'X';
		}
		for (int k = 0; k <= m; k++)
			board[n][k] = 'X';
		printf("%d\n", fill_board(n, m));
	}
	return 0;
}
